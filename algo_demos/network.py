import socket

def addr(rank, machine=126):
    return f'tcp://{socket.gethostbyname(socket.gethostname())}:{40000 + rank}'

def en(v: str):
    return v.encode('utf8')

def de(v: bytes):
    return v.decode('utf8')
