import json, zmq
import numpy as np
from collections import deque
import network as nt
import random, time

class Node():
    def __init__(self, r: int, topology):
        super().__init__()
        self.rank = r
        self.ml = ML(self.rank, len(topology))

        context = zmq.Context()

        self.router = context.socket(zmq.ROUTER)
        self.router.setsockopt(zmq.IDENTITY, nt.en(str(self.rank)))
        self.router.bind(nt.addr(self.rank))
        self.router.probe_router = 1
    
        self.buffer = deque()
        self.neighbors = []
        self.degree = 0
        for i in range(len(topology)):
            if topology[self.rank][i]:
                self.degree += 1
                self.neighbors.append(i)
                self.router.connect(nt.addr(i))

    def send_model(self):
        n = random.choice(self.neighbors)
        data = self.ml.serialize()
        meta = {}
        j = {'meta': json.dumps(meta), 'data': json.dumps(data)}
        self.router.send_multipart([nt.en(str(n)), nt.en(json.dumps(j))])

    def loop(self):
        while True:
            _, recv = self.router.recv_multipart()
            if not recv:
                continue # Probe messages
            j = json.loads(nt.de(recv))
            meta, data = json.loads(j['meta']), json.loads(j['data'])
            self.buffer.append((meta, ML.deserialize(data)))
            while len(self.buffer) != 0:
                meta, data = self.buffer.popleft()
                state_dict = self.ml.net.state_dict()
                for key in state_dict.keys():
                    state_dict[key] = data[key]
                self.ml.epoch()
                self.send_model()
    
def start(rank, topology):
    node = Node(rank, topology)
    time.sleep(1)
    node.ml.epoch()
    node.send_model()
    node.loop()

import torch
from torch import nn, utils, optim
import torch.nn.functional as F
from torchvision import transforms, datasets

BATCH_SIZE = 64
EPOCHS = 5

class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(28 * 28, 64)
        self.fc2 = nn.Linear(64, 32)
        self.fc3 = nn.Linear(32, 10)
    
    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return F.log_softmax(x, dim=1)

class ML():
    def __init__(self, rank, world_size):
        super().__init__()
        torch.manual_seed(0)
        np.random.seed(0)

        traindata = datasets.MNIST('data', train=True, download=True,
                                        transform=transforms.Compose([
                                            transforms.ToTensor()
                                        ]))
        testdata = datasets.MNIST('data', train=False, download=True,
                                            transform=transforms.Compose([
                                                transforms.ToTensor()
                                            ]))
        rand_perm = np.arange(len(traindata))
        np.random.shuffle(rand_perm)

        nb_per_node = len(traindata) // world_size
        offset = rank * nb_per_node

        train_subset = utils.data.Subset(traindata, rand_perm[offset:offset+nb_per_node])
        self.trainset = utils.data.DataLoader(train_subset, batch_size=BATCH_SIZE, shuffle=True, drop_last=True)

        self.testset = utils.data.DataLoader(testdata, batch_size=BATCH_SIZE, shuffle=False, drop_last=True)

        self.net = Net()
        self.optim = optim.Adam(self.net.parameters(), lr=0.001)

        self.rank = rank

    def epoch(self):
        for data in self.trainset:
            X, y = data
            self.net.zero_grad()
            output = self.net(X.view(-1, 28 * 28))
            loss = F.nll_loss(output, y)
            loss.backward()
            self.optim.step()
        self.test()

    def test(self):
        correct, total =  0, 0
        with torch.no_grad():
            for data in self.testset:
                X, y = data
                output = self.net(X.view(-1, 28 * 28))
                for idx, i in enumerate(output):
                    if torch.argmax(i) == y[idx]:
                        correct += 1
                    total += 1

        print(f'Node {self.rank} accuracy: {correct / total:.2f}')

    def serialize(self, bias=1.0):
        j = {}
        for key, value in self.net.state_dict().items():
            j[key] = json.dumps((bias * (value.numpy())).tolist())
        
        return j
    
    def deserialize(j):
        state_dict = dict()
        for key, value in j.items():
            state_dict[key] = torch.from_numpy(np.array(json.loads(value)))

        return state_dict
