import torch
from torch import nn, utils, optim
import torch.nn.functional as F
from torchvision import transforms, datasets
import numpy as np
import json

BATCH_SIZE = 64
EPOCHS = 5

class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(28 * 28, 64)
        self.fc2 = nn.Linear(64, 32)
        self.fc3 = nn.Linear(32, 10)
    
    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return F.log_softmax(x, dim=1)

class ML():
    def __init__(self, rank, world_size):
        super().__init__()
        torch.manual_seed(0)
        np.random.seed(0)

        traindata = datasets.MNIST('data', train=True, download=True,
                                        transform=transforms.Compose([
                                            transforms.ToTensor()
                                        ]))
        testdata = datasets.MNIST('data', train=False, download=True,
                                            transform=transforms.Compose([
                                                transforms.ToTensor()
                                            ]))
        rand_perm = np.arange(len(traindata))
        np.random.shuffle(rand_perm)

        nb_per_node = len(traindata) // world_size
        offset = rank * nb_per_node

        train_subset = utils.data.Subset(traindata, rand_perm[offset:offset+nb_per_node])
        trainset = utils.data.DataLoader(train_subset, batch_size=BATCH_SIZE, shuffle=True, drop_last=True)
        self.trainset = list()
        for data in trainset: # TODO can this loop be avoided
            X, y = data
            self.trainset.append((X, y))

        self.testset = utils.data.DataLoader(testdata, batch_size=BATCH_SIZE, shuffle=False, drop_last=True)

        self.net = Net()
        self.optim = optim.Adam(self.net.parameters(), lr=0.001)

        self.rank = rank
        self.index = 0
        self.epoch = 0

    def step(self):
        X, y = self.trainset[self.index]
        self.net.zero_grad()
        output = self.net(X.view(-1, 28 * 28))
        loss = F.nll_loss(output, y)
        loss.backward()
        self.optim.step()
        self.index += 1
        print(f'Node {self.rank} step {self.index} of epoch {self.epoch + 1}')
        if self.index == len(self.trainset):
            self.test()
            self.epoch += 1
            self.index = 0

    def test(self):
        correct, total =  0, 0
        with torch.no_grad():
            for data in self.testset:
                X, y = data
                output = self.net(X.view(-1, 28 * 28))
                for idx, i in enumerate(output):
                    if torch.argmax(i) == y[idx]:
                        correct += 1
                    total += 1

        print(f'Node {self.rank}: accuracy after {self.epoch} epochs: {correct / total:.2f}')

    def serialize(self, bias=1.0):
        j = {}
        for key, value in self.net.state_dict().items():
            j[key] = json.dumps((bias * (value.numpy())).tolist())
        
        return j
    
    def deserialize(j):
        state_dict = dict()
        for key, value in j.items():
            state_dict[key] = torch.from_numpy(np.array(json.loads(value)))

        return state_dict
