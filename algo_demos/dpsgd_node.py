import json, zmq, time
from collections import deque
import ml
import network as nt

class Node():
    def __init__(self, r: int, topology):
        super().__init__()
        self.rank = r
        self.ml = ml.ML(self.rank, len(topology))

        context = zmq.Context()

        self.router = context.socket(zmq.ROUTER)
        self.router.setsockopt(zmq.IDENTITY, str(self.rank).encode())
        self.router.bind(nt.addr(self.rank))
        self.router.probe_router = 1

        self.buffer = dict()
        for i in range(len(topology)):
            if topology[self.rank][i]:
                neighbor = i
                self.buffer[neighbor] = deque()
                self.router.connect(nt.addr(neighbor))

    def send_data(self):
        data = self.ml.serialize()
        for n in self.buffer.keys():
            meta = {'degree': len(self.buffer)}
            j = {'meta': json.dumps(meta), 'data': json.dumps(data)}
            self.router.send_multipart([str(n).encode(), json.dumps(j).encode()])

    def loop(self):
        while True:
            sender, recv = self.router.recv_multipart()
            if not recv:
                continue # Probe messages

            j = json.loads(nt.de(recv))
            meta, data = json.loads(j['meta']), json.loads(j['data'])
            self.buffer[int(sender.decode())].append((meta, ml.ML.deserialize(data)))

            if self.check_and_average():
                self.ml.step()
                self.send_data()
    
    def check_and_average(self):
        for n in self.buffer:
            if len(self.buffer[n]) == 0:
                return False

        total = dict()
        weight_total = 0
        for n in self.buffer:
            meta, data = self.buffer[n].popleft()
            weight = 1/(max(len(self.buffer), int(meta['degree'])) + 1) # Metro-Hastings
            weight_total += weight
            for key, value in data.items():
                if key in total:
                    total[key] += value * weight
                else:
                    total[key] = value * weight

        state_dict = self.ml.net.state_dict()
        for key, value in state_dict.items():
            state_dict[key] = (1 - weight_total) * value + total[key] # Metro-Hastings

        return True

def start(rank, topology):
    node = Node(rank, topology)
    time.sleep(1)
    node.send_data()
    node.loop()
