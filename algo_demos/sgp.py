from torch.multiprocessing import spawn, freeze_support
import sgp_node as node
import numpy as np

def main():
    HEIGHT = 3
    WIDTH = 4

    topology = np.identity(HEIGHT * WIDTH, dtype=float)
    for row in range(HEIGHT):
        for col in range(WIDTH):
            r = rank(row, col, WIDTH)
            if row != 0:
                connect(r, rank(row - 1, col, WIDTH), topology)
            if row != HEIGHT - 1:
                connect(r, rank(row + 1, col, WIDTH), topology)
            if col != 0:
                connect(r, rank(row, col - 1, WIDTH), topology)
            if col != WIDTH - 1:
                connect(r, rank(row, col + 1, WIDTH), topology)

    for i in range(len(topology)):
        topology[i] = topology[i] / topology[i].sum() # equal weights

    spawn(fn=node.start, args=(np.array(topology),), nprocs=len(topology))

def rank(row, col, cols):
    return row * cols + col

def connect(a, b, topology):
    topology[a][b] = 1
    topology[b][a] = 1

if __name__ == '__main__':
    freeze_support()
    main()
