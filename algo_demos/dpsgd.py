from torch.multiprocessing import spawn, freeze_support
import dpsgd_node as node
import numpy as np
import networkx as nx

def main():
    NODES = 5
    EDGES = 10

    graph = nx.gnm_random_graph(NODES, EDGES, 0)
    topology = np.array(nx.to_numpy_matrix(graph, dtype=int))

    print(topology)

    spawn(fn=node.start, args=(topology,), nprocs=NODES)

if __name__ == '__main__':
    freeze_support()
    main()
