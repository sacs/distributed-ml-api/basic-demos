import json, zmq
from collections import deque
import ml
import network as nt

class Node():
    def __init__(self, r: int, topology):
        super().__init__()
        self.rank = r
        self.ml = ml.ML(self.rank, len(topology))
        self.bias = 1.0

        context = zmq.Context()

        self.router = context.socket(zmq.ROUTER)
        self.router.setsockopt(zmq.IDENTITY, nt.en(str(self.rank)))
        self.router.bind(nt.addr(self.rank))
        self.router.probe_router = 1
    
        self.buffer = dict()
        self.weights = topology[self.rank]
        for i in range(len(topology)):
            if topology[self.rank][i] > 0.0001:
                neighbor = i
                self.buffer[neighbor] = deque()
                self.router.connect(nt.addr(neighbor))

    def send_data(self):
        data = self.ml.serialize(self.bias)
        for n in self.buffer.keys():
            meta = {'rank': self.rank, 'weight': self.weights[n], 'bias': self.bias}
            j = {'meta': json.dumps(meta), 'data': json.dumps(data)}
            self.router.send_multipart([nt.en(str(n)), nt.en(json.dumps(j))])

    def loop(self):
        while True:
            print('hey')
            _, recv = self.router.recv_multipart()
            if not recv:
                continue # Probe messages

            j = json.loads(nt.de(recv))
            meta, data = json.loads(j['meta']), json.loads(j['data'])
            self.buffer[int(meta['rank'])].append((meta, ml.ML.deserialize(data)))

            while self.check_and_average():
                self.ml.step()
                self.send_data()

    def check_and_average(self):
        for n in self.buffer:
            if len(self.buffer[n]) == 0:
                return False
        
        total = dict()
        weight_total = 0
        bias_total = 0
        for n in self.buffer:
            meta, data = self.buffer[n].popleft()
            weight = float(meta['weight'])
            bias = float(meta['bias'])

            weight_total += weight
            bias_total += weight * bias
            for key, value in data.items():
                if key in total:
                    total[key] += value * weight
                else:
                    total[key] = value * weight


        bias_total += self.weights[self.rank] * self.bias
        state_dict = self.ml.net.state_dict()
        for key, value in state_dict.items():
            total[key] += self.weights[self.rank] * value

        self.bias = bias_total
        for key, value in total.items():
            state_dict[key] = value / self.bias

        return True

def start(rank, topology):
    node = Node(rank, topology)
    node.ml.step()
    node.send_data()
    node.loop()
