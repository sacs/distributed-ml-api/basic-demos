import base64

def addr(rank):
    return f'tcp://127.0.0.1:{40000 + rank}'

def en(v: str):
    return v.encode('utf8')

def de(v: bytes):
    return v.decode('utf8')
