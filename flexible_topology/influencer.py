import network as nt
import sys
import zmq
import json

PORT = 39999

def main():
    target = int(sys.argv[1])
    new_neighbors = sys.argv[2:]

    context = zmq.Context()
    router = context.socket(zmq.ROUTER)
    router.setsockopt(zmq.IDENTITY, nt.en('influencer'))
    router.bind(nt.addr(PORT))
    router.probe_router = 1
    router.connect(nt.addr(target))
    msg = router.recv_multipart()
    print(msg)

    router.send_multipart([nt.en(str(target)), nt.en(json.dumps({'new_neighbors': new_neighbors}))])
    print(f'Influencer: Node {target}\'s topology has been changed; my work here is done.')

if __name__ == '__main__':
    main()
