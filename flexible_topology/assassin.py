import network as nt
import sys
import zmq

PORT = 39999

def main():
    target = int(sys.argv[1])
    context = zmq.Context()
    router = context.socket(zmq.ROUTER)
    router.setsockopt(zmq.IDENTITY, nt.en('assassin'))
    router.bind(nt.addr(PORT))
    router.probe_router = 1
    router.connect(nt.addr(target))
    msg = router.recv_multipart()
    print(msg)
    msg[1] = b'die'
    router.send_multipart(msg)
    print(f'Assassin: Node {target} has been eliminated; my work here is done.')

if __name__ == '__main__':
    main()
