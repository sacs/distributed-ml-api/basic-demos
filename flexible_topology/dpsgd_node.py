import json, zmq
from collections import deque
import ml
import network as nt
import sys, time

class Node():
    def __init__(self, r: int, topology):
        super().__init__()
        self.rank = r
        self.ml = ml.ML(self.rank, len(topology))

        context = zmq.Context()

        self.router = context.socket(zmq.ROUTER)
        self.router.setsockopt(zmq.IDENTITY, nt.en(str(self.rank)))
        self.router.bind(nt.addr(self.rank))
        self.router.probe_router = 1

        self.buffer = dict()
        self.degree = 0
        for i in range(len(topology)):
            if topology[self.rank][i]:
                self.degree += 1
                neighbor = i
                self.buffer[neighbor] = deque()
                self.router.connect(nt.addr(neighbor))

    def send_model_to_everyone(self):
        for n in self.buffer.keys():
            self.send_model_to(nt.en(str(n)), self.serialize())

    def serialize(self):
        data = self.ml.serialize()
        meta = {'rank': self.rank, 'degree': self.degree}
        j = {'meta': json.dumps(meta), 'data': json.dumps(data)}
        return nt.en(json.dumps(j))

    def send_model_to(self, id, to_send):
        self.router.send_multipart([id, to_send])

    def disconnect(self, neighbors):
        for n in neighbors:
            self.router.send_multipart([nt.en(str(n)), nt.en(json.dumps({'bye-rank': self.rank}))])

    def connect(self, neighbors):
        for n in neighbors:
            self.router.connect(nt.addr(n))
            print(f'{self.rank} adding new neighbor : {n}')

    def loop(self):
        while True:
            sender, recv = self.router.recv_multipart()
            if not recv: # Probe messages
                sender = nt.de(sender)
                if sender == 'influencer' or sender == 'assassin':
                    continue
                sender = int(sender)
                if not sender in self.buffer.keys():
                    self.buffer[sender] = deque()
                    self.send_model_to(nt.en(str(sender)), self.serialize())
                    print(f'{self.rank} new neighbor: {sender}')
                continue

            if sender == nt.en('assassin') and recv == nt.en('die'):
                print(f'Node {self.rank} initiating self destruct, so long!')
                self.disconnect(self.buffer.keys())
                sys.exit(0)

            if sender == nt.en('influencer'):
                recv = json.loads(nt.de(recv))
                print(f'Node {self.rank} changing neighbors!')
                new_neighbors = set(map(int, recv['new_neighbors']))
                to_disconnect = self.buffer.keys() - new_neighbors
                to_connect = new_neighbors - self.buffer.keys()
                self.disconnect(to_disconnect)
                for key in to_disconnect:
                    self.buffer.pop(key)
                self.connect(to_connect)
                for key in to_connect:
                    self.buffer[key] = deque()

                continue
            recv = json.loads(nt.de(recv))
            if 'bye-rank' in recv:
                print(f'{self.rank} received goodbye from {recv["bye-rank"]}')
                self.buffer.pop(recv['bye-rank'])
            else:
                meta, data = json.loads(recv['meta']), json.loads(recv['data'])
                rank = int(meta['rank'])
                if rank in self.buffer.keys():
                    self.buffer[rank].append((meta, ml.ML.deserialize(data)))

            while self.check_and_average():
                self.ml.step()
                self.send_model_to_everyone()

    def check_and_average(self):
        for n in self.buffer:
            if len(self.buffer[n]) == 0:
                self.waiting_on = n
                return False

        total = dict()
        weight_total = 0
        for n in self.buffer:
            meta, data = self.buffer[n].popleft()
            weight = 1/(max(self.degree, int(meta['degree'])) + 1) # Metro-Hastings
            weight_total += weight
            for key, value in data.items():
                if key in total:
                    total[key] += value * weight
                else:
                    total[key] = value * weight

        state_dict = self.ml.net.state_dict()
        for key, value in state_dict.items():
            if key in total:
                total[key] += value * (1 - weight_total)
            else:
                total[key] = value * (1 - weight_total)

        #TODO do i need torch no grad
        for key, value in total.items():
            state_dict[key] = value

        return True

def start(rank, topology):
    node = Node(rank, topology)
    time.sleep(1)
    node.ml.step()
    node.send_model_to_everyone()
    node.loop()
