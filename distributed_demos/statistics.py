import jsonlines, torch, sys, os
from torchvision import transforms, datasets
from math import sqrt
import ml

def main():
    FOLDER = sys.argv[1]
    net = ml.Net()
    testdata = datasets.MNIST('data', train=False, download=True,
                                    transform=transforms.Compose([
                                        transforms.ToTensor()
                                    ]))
    testset = torch.utils.data.DataLoader(testdata, shuffle=False)
    stats_filename = os.path.join('logging', FOLDER + '_stats.txt')
    if not os.path.exists(stats_filename):
        with open(stats_filename, 'x'):
            pass

    nets = {} # rank:list(model)
    accuracies = {} # rank:list(accuracy)
    min_epoch = 1000

    for name in os.listdir(os.path.join('logging', FOLDER)):
        if not 'jsonl' in name:
            continue
        rank = int(name.split('.')[0])
        nets[rank] = []
        accuracies[rank] = []

        filename = os.path.join('logging', FOLDER, name)
        epoch = 0
        with jsonlines.open(filename) as reader:
            for d in reader:
                net = ml.Net()
                data = ml.ML.deserialize(d)
                for key, value in data.items():
                    net.state_dict()[key].copy_(value)

                nets[rank].append(net)
                epoch += 1

        min_epoch = min(min_epoch, epoch)
    
    with open(stats_filename, 'w') as out:
        for epoch in range(min_epoch):
            average_net = ml.Net()
            average_state_dict = average_net.state_dict()
            for key, value in average_state_dict.items():
                average_state_dict[key].copy_(value * 0)

            for rank, node_nets in nets.items():
                for key, value in node_nets[epoch].state_dict().items():
                    average_state_dict[key].copy_(average_state_dict[key] + value)

            
            for key, value in average_state_dict.items():
                average_state_dict[key].copy_(value / len(nets))

            acc = test(average_net, testset)
            out.write(f'Average model\'s accuracy after {epoch} epoch(s) was {acc*100:.2f}\n')
            print('average', epoch)

            distances = {} # rank:distance
            avg_dist = 0

            for rank, node_nets in nets.items():
                distance = 0
                for key, value in node_nets[epoch].state_dict().items():
                    diff = value - average_state_dict[key]
                    distance += torch.sum(diff * diff)
                
                distance = sqrt(distance)
                distances[rank] = distance
                avg_dist += distance

            avg_dist /= len(nets)

            std_dist = 0
            for rank, dist in distances.items():
                std_dist += (dist - avg_dist) ** 2
            
            std_dist = sqrt(std_dist / len(nets))

            out.write(f'After {epoch} epoch(s), average distance was {avg_dist:.3f} & std was {std_dist:.3f}\n')
            print('std', epoch)

def test(net, testset):
    correct, total =  0, 0
    for data in testset:
        X, y = data
        output = net(X.view(-1, 28 * 28))
        for idx, i in enumerate(output):
            if torch.argmax(i) == y[idx]:
                correct += 1
            total += 1
    return correct/total

if __name__ == '__main__':
    main()
