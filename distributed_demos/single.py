import ml

class Node():
    def __init__(self, rank, nprocs, machine_id, machines, folder, dataset, homogenous):
        super().__init__()
        self.ml = ml.ML(machine_id * nprocs + rank, machines * nprocs, folder, dataset, homogenous)
    
    def loop(self):
        while True:
            self.ml.step()

def start(rank, nprocs, machine_id, machines, folder, dataset, homogenous):
    Node(rank, nprocs, machine_id, machines, folder, dataset, homogenous).loop()
