import json, zmq, os
from collections import deque
import ml
import network as nt

def neighbors(nprocs, machines, rank, machine_id):
    neighbors = set()
    for r in range(nprocs):
        if r != rank:
            neighbors.add((r, machine_id))
    for m in range(machines):
        if m != machine_id:
            neighbors.add((rank, m))
    return neighbors

class Node():
    def __init__(self, r, nprocs, machine_id, machines, folder, dataset, homogenous):
        super().__init__()
        self.rank = int(r)
        self.machine = int(machine_id)
        self.identity = nt.identity(self.rank, self.machine).encode()
        self.ml = ml.ML(self.machine * nprocs + self.rank, machines * nprocs, folder, dataset, homogenous)

        context = zmq.Context()

        self.router = context.socket(zmq.ROUTER)
        self.router.setsockopt(zmq.IDENTITY, self.identity)
        self.router.bind(nt.addr(self.rank, self.machine))

        ns = neighbors(nprocs, machines, self.rank, self.machine)
        self.neighbors = dict()
        self.barrier = {}
        self.peer_sockets = {}

        for tuple in ns:
            rank, machine = tuple
            id = nt.identity(rank, machine).encode()
            self.barrier[id] = False
            req = context.socket(zmq.DEALER)
            req.setsockopt(zmq.IDENTITY, self.identity)
            req.probe_router = 1
            req.connect(nt.addr(rank, machine))
            self.peer_sockets[id] = req
            self.neighbors[id] = deque()

        self.sent = 0
        self.recieved = 0
        self.ntfile = os.path.join('logging', folder, f'{self.rank}network.txt')

    def send_data(self):
        data = self.ml.serialize()
        for id, sock in self.peer_sockets.items():
            meta = {'degree': len(self.neighbors)}
            j = {'meta': json.dumps(meta), 'data': json.dumps(data)}
            sock.send(json.dumps(j).encode())

        self.sent += len(self.neighbors.keys()) * len(json.dumps(data).encode())

    def loop(self):
        while True:
            sender, recv = self.router.recv_multipart()
            if not recv: # Probe
                assert sender in self.barrier
                self.barrier[sender] = True
                if self.all_connected():
                    self.send_data()
                continue

            self.recieved += len(recv)

            j = json.loads(nt.de(recv))
            meta, data = json.loads(j['meta']), json.loads(j['data'])
            self.neighbors[sender].append((meta, ml.ML.deserialize(data)))

            if self.check_and_average():
                epoch_over = self.ml.step()
                self.send_data()
                # if epoch_over:
                #     self.log_network()
    
    def all_connected(self):
        for _, val in self.barrier.items():
            if not val:
                return False
        return True

    def check_and_average(self):
        for n in self.neighbors:
            if len(self.neighbors[n]) == 0:
                return False

        total = dict()
        weight_total = 0
        for n in self.neighbors:
            meta, data = self.neighbors[n].popleft()
            weight = 1/(max(len(self.neighbors), int(meta['degree'])) + 1) # Metro-Hastings
            weight_total += weight
            for key, value in data.items():
                if key in total:
                    total[key] += value * weight
                else:
                    total[key] = value * weight

        state_dict = self.ml.net.state_dict()
        for key, value in state_dict.items():
            state_dict[key].copy_((1 - weight_total) * value + total[key]) # Metro-Hastings

        return True

    def log_network(self):
        if not os.path.exists(self.ntfile):
            with open(self.ntfile, 'x'):
                pass
        with open(self.ntfile, 'a') as file:
            file.write(f'After {self.ml.epoch}: sent {self.sent} byes; received {self.recieved} bytes')

def start(rank, nprocs, machine_id, machines, folder, dataset, homogenous):
    Node(rank, nprocs, machine_id, machines, folder, dataset, homogenous).loop()
