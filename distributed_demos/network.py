def addr(rank, machine_id):
    return f'tcp://127.0.0.1:{20000 + rank}'
    # return f'tcp://10.90.41.{machine_id + 128}:{20000 + rank}'

def en(v: str):
    return v.encode('utf8')

def de(v: bytes):
    return v.decode('utf8')

def identity(rank, machine):
    return f'{machine}-{rank}'

def unpack_identity(identity):
    return identity.split('-')
