import json, zmq
import network as nt
import random, os
import ml

def neighbors(nprocs, machines, rank, machine_id):
    neighbors = set()
    for r in range(nprocs):
        if r != rank:
            neighbors.add((r, machine_id))
    for m in range(machines):
        if m != machine_id:
            neighbors.add((rank, m))
    return neighbors

class Node():
    def __init__(self, r, nprocs, machine, machines, folder, dataset, homogenous):
        super().__init__()
        self.rank = int(r)
        self.machine = int(machine)
        self.identity = nt.identity(self.rank, self.machine).encode()
        self.ml = ml.ML(machine * nprocs + self.rank, machines * nprocs, folder, dataset, homogenous)

        context = zmq.Context()

        self.router = context.socket(zmq.ROUTER)
        self.router.setsockopt(zmq.IDENTITY, self.identity)
        self.router.bind(nt.addr(self.rank, self.machine))

        ns = neighbors(nprocs, machines, self.rank, self.machine)
        self.barrier = {}
        self.peer_sockets = {}

        for tuple in ns:
            rank, machine = tuple
            id = nt.identity(rank, machine).encode()
            self.barrier[id] = False
            req = context.socket(zmq.DEALER)
            req.setsockopt(zmq.IDENTITY, self.identity)
            req.probe_router = 1
            req.connect(nt.addr(rank, machine))
            self.peer_sockets[id] = req

        self.sent = 0
        self.recieved = 0
        self.ntfile = os.path.join('logging', folder, f'{self.rank}network.txt')

    def send_model(self):
        data = self.ml.serialize()
        meta = {}
        j = {'meta': json.dumps(meta), 'data': json.dumps(data)}
        random.choice(list(self.peer_sockets.values())).send(json.dumps(j).encode())
        self.sent += len(json.dumps(data).encode())

    def loop(self):
        while True:
            sender, recv = self.router.recv_multipart()
            if not recv: # Probe
                assert sender in self.barrier
                self.barrier[sender] = True
                if self.all_connected():
                    self.send_model()
                continue

            self.recieved += len(recv)

            j = json.loads(nt.de(recv))
            meta, data = json.loads(j['meta']), json.loads(j['data'])
            data = ml.ML.deserialize(data)

            state_dict = self.ml.net.state_dict()
            for key, value in state_dict.items():
                state_dict[key].copy_((value + data[key]) / 2)

            epoch_over = self.ml.step()

            self.send_model()
            # if epoch_over:
            #     self.log_network()
    
    def all_connected(self):
        for _, val in self.barrier.items():
            if not val:
                return False
        return True

    def log_network(self):
        if not os.path.exists(self.ntfile):
            with open(self.ntfile, 'x'):
                pass
        with open(self.ntfile, 'a') as file:
            file.write(f'After {self.ml.epoch}: sent {self.sent} byes; received {self.recieved} bytes\n')

def start(rank, nprocs, machine_id, machines, folder, dataset, homogenous):
    Node(rank, nprocs, machine_id, machines, folder, dataset, homogenous).loop()
