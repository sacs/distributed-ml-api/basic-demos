import torch
from torch import nn, utils, optim
import torch.nn.functional as F
from torchvision import transforms, datasets
import numpy as np
import json, os, jsonlines
from functools import reduce

BATCH_SIZE = 128
LEARNING_RATE = 0.01
MOMENTUM = 0.9

class MNISTNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(28 * 28, 10)

    def forward(self, x):
        x = self.fc1(x)
        return F.log_softmax(x, dim=1)

class CifarNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(3 * 32 * 32, 10)

    def forward(self, x):
        x = self.fc1(x)
        return F.log_softmax(x, dim=1)

class CifarNetCNN(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 6, 5, 2)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5, 2)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return F.log_softmax(x, dim=1)

class ML():
    def __init__(self, rank, world_size, folder, dataset_name, homogenous=True):
        super().__init__()
        torch.manual_seed(0)
        np.random.seed(0)
        self.rank = int(rank)
        self.net = None
        self.dataset = dataset_name

        traindata, testdata, train_subset = None, None, None
        if dataset_name == 'mnist':
            traindata = datasets.MNIST('data', train=True, transform=transforms.ToTensor())
            testdata = datasets.MNIST('data', train=False, transform=transforms.ToTensor())
            self.net = MNISTNet()
        elif dataset_name == 'cifar10':
            transform = transforms.Compose([transforms.ToTensor(),
                                        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
            traindata = datasets.CIFAR10('data', train=True, transform=transform)
            testdata = datasets.CIFAR10('data', train=False, transform=transform)
            self.net = CNN()
        else:
            raise ValueError('unrecognized dataset')

        if homogenous:
            rand_perm = np.arange(len(traindata))
            np.random.shuffle(rand_perm)

            nb_per_node = len(traindata) // world_size
            offset = rank * nb_per_node

            train_subset = utils.data.Subset(traindata, rand_perm[offset:offset+nb_per_node])
        else:
            idx = torch.tensor(traindata.targets) == rank
            train_subset = utils.data.Subset(traindata, np.where(idx==1)[0])

        trainset = utils.data.DataLoader(train_subset, batch_size=BATCH_SIZE, shuffle=True, drop_last=True)
        self.trainset = list(trainset)

        self.testset = utils.data.DataLoader(testdata, batch_size=BATCH_SIZE, shuffle=False, drop_last=True)
        self.optim = optim.SGD(self.net.parameters(), lr=LEARNING_RATE, momentum=MOMENTUM)

        self.index = 0
        self.epoch = 0
        self.filename = os.path.join('logging', folder, f'{self.rank}.jsonl')

    def step(self):
        X, y = self.trainset[self.index]
        self.net.zero_grad()
        output = None
        if self.dataset == 'mnist':
            output = self.net(X.view(BATCH_SIZE, -1))
        elif self.dataset == 'cifar10':
            output = self.net(X)

        loss = F.nll_loss(output, y)
        loss.backward()
        self.optim.step()
        self.index += 1
        print(f'Node {self.rank} step {self.index} of epoch {self.epoch}')
        if self.index == len(self.trainset):
            self.epoch += 1
            self.index = 0
            # self.log()
            self.test()
            return True
        else:
            return False

    def log(self):
        if not os.path.exists(self.filename):
            with open(self.filename, 'w') as file:
                pass

        with jsonlines.open(self.filename, mode='a') as file:
            file.write(self.serialize())

    def test(self):
        correct, total =  0, 0
        with torch.no_grad():
            for data in self.testset:
                X, y = data
                output = None
                if self.dataset == 'mnist':
                    output = self.net(X.view(BATCH_SIZE, -1))
                elif self.dataset == 'cifar10':
                    output = self.net(X)
                for idx, i in enumerate(output):
                    if torch.argmax(i) == y[idx]:
                        correct += 1
                    total += 1

        print(f'Node {self.rank}: accuracy after {self.epoch} epochs: {correct / total:.2f}')

    def serialize(self, bias=1.0):
        j = {}
        for key, value in self.net.state_dict().items():
            j[key] = json.dumps((bias * (value.numpy())).tolist())
        
        return j

    def deserialize(j):
        state_dict = dict()
        for key, value in j.items():
            state_dict[key] = torch.from_numpy(np.array(json.loads(value)))

        return state_dict
