from torch.multiprocessing import spawn, freeze_support, cpu_count
import dpsgd_node, rw_node, single
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--machine_id', type=int, default=0)
    parser.add_argument('-ms', '--machines', type=int, default=1)
    parser.add_argument('-ps', '--procs', type=int)
    parser.add_argument('-f', '--folder_name', type=str)
    args = parser.parse_args()
    print(args)

    node = None
    if args.algorithm == 'dpsgd':
        node = dpsgd_node
    elif args.algorithm == 'gossip':
        node = rw_node
    elif args.algorithm == 'single':
        node = single
    else:
        ValueError('unrecognized algorithm')

    spawn(fn=node.start, args=( args.procs, args.machine_id,
                                args.machines, args.folder_name,
                                args.dataset, args.homogenous),
                            nprocs=args.procs)

if __name__ == '__main__':
    freeze_support()
    main()
