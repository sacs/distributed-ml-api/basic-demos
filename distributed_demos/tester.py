import jsonlines, torch, sys, os
from torchvision import transforms, datasets
import ml

def main():
    FOLDER = sys.argv[1]
    net = ml.CifarNetCNN()
    transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    testdata = datasets.CIFAR10('data', train=False, download=True, transform=transform)
    testset = torch.utils.data.DataLoader(testdata, batch_size=ml.BATCH_SIZE, shuffle=False, drop_last=True)

    results_filename = os.path.join('logging', FOLDER + '_results.txt')
    if not os.path.exists(results_filename):
        with open(results_filename, 'x'):
            pass

    with open(results_filename, 'w') as out:
        for name in os.listdir(os.path.join('logging', FOLDER)):
            if not 'jsonl' in name:
                continue
            rank = int(name.split('.')[0])
            filename = os.path.join('logging', FOLDER, name)
            epoch = 1
            with jsonlines.open(filename) as reader:
                for data in reader:
                    state_dict = net.state_dict()
                    for key, value in ml.ML.deserialize(data).items():
                        state_dict[key].copy_(value)
                    accuracy = test(net, testset)
                    out.write(f'Node {rank}\'s accuracy after {epoch} epoch(s) was {accuracy*100:.2f}\n')
                    print(rank, epoch)
                    epoch += 1
            out.write('-----------------------------------------------------------\n')

def test(net, testset):
    correct, total =  0, 0
    for data in testset:
        X, y = data
        output = net(X)
        for idx, i in enumerate(output):
            if torch.argmax(i) == y[idx]:
                correct += 1
            total += 1
    return correct/total

if __name__ == '__main__':
    main()
