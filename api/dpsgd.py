import json, zmq, time
from collections import deque
import util
from torch.multiprocessing import spawn

class DPSGDNode():
    def __init__(self, rank, machine_id, procs_per_machine, machines, local_updates, ML, NT, epochs, *args):
        super().__init__()
        self.rank = int(rank)
        self.machine = int(machine_id)
        self.local_updates = int(local_updates)
        self.epochs = int(epochs)
        self.NT = NT
        self.identity = util.node_identity(self.rank, self.machine).encode()
        self.ml = ML(self.machine * procs_per_machine + self.rank, machines * procs_per_machine, list(args)[0])

        self.context = zmq.Context()

        self.router = self.context.socket(zmq.ROUTER)
        self.router.setsockopt(zmq.IDENTITY, self.identity)
        self.router.bind(NT.addr(self.rank, self.machine))

        neighbors = NT.neighbors(self.rank, self.machine, procs_per_machine, machines)
        self.peer_deques = dict()
        self.barrier = {}
        self.peer_sockets = {}
        self.sent_disconnections = False

        for tuple in neighbors:
            rank, machine = tuple
            id = util.node_identity(rank, machine).encode()
            self.barrier[id] = False
            req = self.context.socket(zmq.DEALER)
            req.setsockopt(zmq.IDENTITY, self.identity)
            req.probe_router = 1
            req.connect(NT.addr(rank, machine))
            self.peer_sockets[id] = req
            self.peer_deques[id] = deque()

    def __del__(self):
        self.context.destroy(linger=0)

    def send_model(self):
        data = util.serialize_model(self.ml.get_net())
        for sock in self.peer_sockets.values():
            meta = {'degree': len(self.peer_sockets)}
            j = {'meta': json.dumps(meta), 'data': json.dumps(data)}
            sock.send(self.NT.encrypt(json.dumps(j)))

    def nb_connected(self):
        counter = 0
        for val in self.barrier.values():
            if val:
                counter += 1
        return counter

    def disconnect(self):
        if not self.sent_disconnections:
            for sock in self.peer_sockets.values():
                sock.send(b'OVER')
            self.sent_disconnections = True

    def loop(self):
        while True:
            sender, recv = self.router.recv_multipart()
            if not recv: # Probe
                assert sender in self.barrier
                self.barrier[sender] = True
                if self.nb_connected() == len(self.barrier):
                    self.send_model()
                continue

            if recv == b'OVER':
                self.barrier[sender] = False
                if self.nb_connected() == 0:
                    return
                else:
                    continue

            if sender not in self.peer_sockets:
                continue

            j = json.loads(self.NT.decrypt(recv))
            meta, data = json.loads(j['meta']), json.loads(j['data'])
            self.peer_deques[sender].append((meta, util.deserialize_model(data)))

            if self.check_and_average():
                for _ in range(self.local_updates):
                    self.ml.step()
                    if self.ml.get_epoch() == self.epochs:
                        self.disconnect()
                self.send_model()

    def check_and_average(self):
        for n in self.peer_deques:
            if len(self.peer_deques[n]) == 0:
                return False

        total = dict()
        weight_total = 0
        for n in self.peer_deques:
            meta, data = self.peer_deques[n].popleft()
            weight = 1/(max(len(self.peer_deques), int(meta['degree'])) + 1) # Metro-Hastings
            weight_total += weight
            for key, value in data.items():
                if key in total:
                    total[key] += value * weight
                else:
                    total[key] = value * weight

        for key, value in self.ml.get_net().state_dict().items():
            total[key] += (1 - weight_total) * value # Metro-Hastings

        self.ml.get_net().load_state_dict(total)

        return True


def start(rank, machine_id, procs_per_machine, machines, local_updates, ML, NT, epochs, *args):
    DPSGDNode(rank, machine_id, procs_per_machine, machines, local_updates, ML, NT, epochs, list(args)[0]).loop()
    
def run(machine_id, procs_per_machine, machines, local_updates, ML, NT, epochs, *args):
    spawn(fn=start, nprocs=procs_per_machine, 
        args=[machine_id, procs_per_machine, machines, local_updates, ML, NT, epochs] + list(args))
