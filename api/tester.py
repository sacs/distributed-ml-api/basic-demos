import jsonlines, torch, sys, os
from torchvision import transforms, datasets
import MNIST, util

def main():
    FOLDER = sys.argv[1]
    net = MNIST.Net()
    transform = transforms.ToTensor()
    testdata = datasets.MNIST('data', train=False, transform=transforms.ToTensor())
    testset = torch.utils.data.DataLoader(testdata, batch_size=MNIST.BATCH_SIZE, shuffle=False, drop_last=True)

    results_filename = os.path.join('experiments', FOLDER + '_results.txt')
    if not os.path.exists(results_filename):
        with open(results_filename, 'x'):
            pass

    with open(results_filename, 'w') as out:
        for name in os.listdir(os.path.join('experiments', FOLDER)):
            if not 'jsonl' in name:
                continue
            rank = int(name.split('.')[0])
            filename = os.path.join('experiments', FOLDER, name)
            epoch = 1
            with jsonlines.open(filename) as reader :
                for data in reader:
                    state_dict = net.state_dict()
                    for key, value in util.deserialize_model(data).items():
                        state_dict[key].copy_(value)
                    accuracy = test(net, testset)
                    out.write(f'Node {rank}\'s accuracy after {epoch} epoch(s) was {accuracy*100:.2f}\n')
                    print(rank, epoch)
                    epoch += 1
            break

def test(net, testset):
    correct, total =  0, 0
    for data in testset:
        X, y = data
        output = net(X.view(MNIST.BATCH_SIZE, -1))
        for idx, i in enumerate(output):
            if torch.argmax(i) == y[idx]:
                correct += 1
            total += 1
    return correct/total

if __name__ == '__main__':
    main()
