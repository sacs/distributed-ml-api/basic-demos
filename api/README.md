This API lets you train your neural network in a distributed (data parallel) setting.  

You can choose between two algorithms:  

1. Distributed Parallel Stochastic Gradient Descent:    
    The gradients at each node are averaged with every neighbor every "local_updates".
2. Gossip Learning (Random Model Walk):
    Individual models perform random walks in the network and train on local data for
    "local_updates" steps. When a model arrives at a node, it is averaged with the node's
    most recent local model.