import torch, numpy, json

"""
    Defines the identity of a node given its rank and its machineid
"""
def node_identity(rank, machine_id):
    return f'{machine_id}-{rank}'

"""
    Deserializes the given dictionary into a state dictionary
    which can be reconstructed into a model.
"""
def deserialize_model(j: dict):
    state_dict = dict()
    for key, value in j.items():
        state_dict[key] = torch.from_numpy(numpy.array(json.loads(value)))

    return state_dict

"""
    Serializes the given model as a dictionary
    It passes through a conversion to a numpy array to be able to
    deserialize from a json format.
"""
def serialize_model(model: torch.nn.Module):
    j = {}
    for key, value in model.state_dict().items():
        j[key] = json.dumps(value.numpy().tolist())
    
    return j
