import torch
from torch import nn, utils, optim
import torch.nn.functional as F
from torchvision import transforms, datasets
import jsonlines, os, argparse
import numpy as np
import util, dpsgd, gossip, api, topologies

class CifarNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(3 * 32 * 32, 10)

    def forward(self, x):
        x = self.fc1(x)
        return F.log_softmax(x, dim=1)

class LeNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 6, kernel_size=5)
        self.conv2 = nn.Conv2d(6, 16, kernel_size=5)
        self.fc1 = nn.Linear(16*5*5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2)
        x = x.view(x.size(0), -1)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return F.log_softmax(x, dim=1)

BATCH_SIZE = 128
LEARNING_RATE = 0.001

class CIFAR(api.Distributed):
    def __init__(self, rank, world_size, *args):
        super().__init__(rank, world_size)
        torch.manual_seed(0)
        np.random.seed(0)

        self.net = LeNet()
        transform = transforms.ToTensor()
        traindata = datasets.CIFAR10('data', train=True, transform=transform)
        testdata = datasets.CIFAR10('data', train=False, transform=transform)

        rand_perm = np.arange(len(traindata))
        np.random.shuffle(rand_perm)

        nb_per_node = len(traindata) // world_size
        offset = rank * nb_per_node

        train_subset = utils.data.Subset(traindata, rand_perm[offset:offset+nb_per_node])

        trainset = utils.data.DataLoader(train_subset, batch_size=BATCH_SIZE, shuffle=True, drop_last=True)
        self.trainset = list(trainset)

        self.testset = utils.data.DataLoader(testdata, batch_size=BATCH_SIZE, shuffle=False, drop_last=True)
        self.optim = optim.SGD(self.net.parameters(), lr=LEARNING_RATE)

        self.index = 0
        self.epoch = 0
        self.filename = os.path.join(args[0], f'{self.rank}.jsonl')

    def get_epoch(self):
        return self.epoch

    def get_net(self):
        return self.net

    def step(self):
        X, y = self.trainset[self.index]
        self.net.zero_grad()
        output = self.net(X)
        loss = F.cross_entropy(output, y)
        loss.backward()
        self.optim.step()
        self.index += 1
        print(f'Node {self.rank} step {self.index} of epoch {self.epoch}')
        if self.index == len(self.trainset):
            self.epoch += 1
            self.index = 0
            # self.log()
            self.test()

    def log(self):
        if not os.path.exists(self.filename):
            with open(self.filename, 'w') as file:
                pass

        with jsonlines.open(self.filename, mode='a') as file:
            file.write(util.serialize_model(self.net))

    def test(self):
        correct, total =  0, 0
        with torch.no_grad():
            for data in self.testset:
                X, y = data
                output = self.net(X)
                for idx, i in enumerate(output):
                    if torch.argmax(i) == y[idx]:
                        correct += 1
                    total += 1

        print(f'Node {self.rank}: accuracy after {self.epoch} epochs: {correct / total:.4f}')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--machine_id', type=int, default=0)
    parser.add_argument('-ms', '--machines', type=int, default=1)
    parser.add_argument('-ps', '--procs', type=int)
    parser.add_argument('-a', '--algorithm', type=str, default='dpsgd')
    parser.add_argument('-f', '--folder_name', type=str)
    parser.add_argument('-l', '--local_updates', type=int, default=1)
    parser.add_argument('-n', '--network', type=str, default='fc')
    parser.add_argument('-e', '--epochs', type=int, default=5)
    args = parser.parse_args()
    print(args)

    algo = None
    if args.algorithm == 'dpsgd':
        algo = dpsgd
    elif args.algorithm == 'gossip':
        algo = gossip

    network = None
    if args.network == 'fc':
        network = topologies.FCNetwork
    elif args.network == 'ring':
        network = topologies.RINGNetwork

    algo.run(args.machine_id, args.procs, args.machines, args.local_updates, CIFAR,
                network, args.epochs, args.folder_name)

if __name__ == '__main__':
    main()
