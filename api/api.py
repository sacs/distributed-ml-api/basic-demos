class Distributed():
    """
    Defines the data distribution according to the node's rank and the world size
    """
    def __init__(self, rank, world_size):
        super().__init__()
        self.rank = int(rank)
        self.world_size = int(world_size)

    def step(self):
        """ Performs a step of training on the local data. logging/testing can also be done here
            rtype: void
        """
        raise NotImplementedError

    def get_net(self):
        """
            returns: the neural net being used by the class
            rtype: torch.nn.Module
        """
        raise NotImplementedError

    def get_epoch(self):
        """
            returns: the current training epoch
            rtype: int
        """
        raise NotImplementedError


class Network():
    """
        This class defines the network properties:
            1. the communication protocol
            2. the network topology
            3. the encryption/decryption
    """
    def addr(rank, machine_id):
        """
            Gives the binding address for a node's router

            :param rank: node's rank on its machine
            :param machine_id: node's machine in the cluster

            returns: the binding address
            rtype: str
        """
        raise NotImplementedError

    def neighbors(rank, machine_id, procs_per_machine, machines):
        """
            Gives the neighbors of a node

            :param rank: node's rank on its machine
            :param machine_id: node's machine in the cluster
            :param procs_per_machine: the number of nodes on the node's machine
            :param machines: the number of machines in the cluster

            returns: the neighbors
            rtype: set(tuple(rank, machine_id))
        """
        raise NotImplementedError

    def encrypt(s: str):
        """
            Encrypts the given string
            Defaults to utf-8 encoding

            returns: the encrypted string
            rtype: bytes
        """
        return s.encode('utf8')

    def decrypt(b: bytes):
        """
            Decrypts the given bytes
            Defaults to utf-8 decoding

            returns: the decrypted string
            rtype: str
        """
        return b.decode('utf8')
