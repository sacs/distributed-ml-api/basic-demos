import api

class FCNetwork(api.Network):
    def addr(rank, machine_id):
        return f'tcp://127.0.0.1:{20000 + rank}'
    
    def neighbors(rank, machine_id, procs_per_machine, machines):
        neighbors = set()
        for r in range(procs_per_machine):
            if r != rank:
                neighbors.add((r, machine_id))
        return neighbors
    
class RINGNetwork(api.Network):
    def addr(rank, machine_id):
        return f'tcp://127.0.0.1:{20000 + rank}'
    
    def neighbors(rank, machine_id, procs_per_machine, machines):
        neighbors = set()
        neighbors.add(((rank + 1) % procs_per_machine, machine_id))
        neighbors.add(((rank - 1) % procs_per_machine, machine_id))
        return neighbors
