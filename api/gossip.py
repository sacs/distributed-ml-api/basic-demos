import json, zmq, random, time
import util
from torch.multiprocessing import spawn

DISCONNECTED = 0
DONE = 1
CONNECTED = 2

class GossipNode():
    def __init__(self, rank, machine_id, procs_per_machine, machines, local_updates, ML, NT, epochs, *args):
        super().__init__()
        self.rank = int(rank)
        self.machine = int(machine_id)
        self.local_updates = int(local_updates)
        self.epochs = int(epochs)
        self.NT = NT
        self.identity = util.node_identity(self.rank, self.machine).encode()
        self.ml = ML(self.machine * procs_per_machine + self.rank, machines * procs_per_machine, list(args)[0])

        self.context = zmq.Context()

        self.router = self.context.socket(zmq.ROUTER)
        self.router.setsockopt(zmq.IDENTITY, self.identity)
        self.router.bind(NT.addr(self.rank, self.machine))

        neighbors = NT.neighbors(self.rank, self.machine, procs_per_machine, machines)
        self.barrier = {}
        self.peer_sockets = {}

        for tuple in neighbors:
            rank, machine = tuple
            id = util.node_identity(rank, machine).encode()
            self.barrier[id] = 0
            req = self.context.socket(zmq.DEALER)
            req.setsockopt(zmq.IDENTITY, self.identity)
            req.probe_router = 1
            req.connect(NT.addr(rank, machine))
            self.peer_sockets[id] = req

        self.sent_disconnections = False
    
    def __del__(self):
        self.context.destroy(linger=0)

    def send_model(self):
        data = util.serialize_model(self.ml.get_net())
        meta = {}
        j = {'meta': json.dumps(meta), 'data': json.dumps(data)}
        random.choice(list(self.peer_sockets.values())).send(self.NT.encrypt(json.dumps(j)))

    def nb(self, state):
        counter = 0
        for val in self.barrier.values():
            if val == state:
                counter += 1
        return counter

    def notify_completion(self):
        for sock in self.peer_sockets.values():
            sock.send(b'DONE')

    def disconnect(self):
        if not self.sent_disconnections:
            for sock in self.peer_sockets.values():
                sock.send(b'OVER')
            self.sent_disconnections = True

    def loop(self):
        while True:
            sender, recv = self.router.recv_multipart()
            if not recv: # Probe
                assert sender in self.barrier
                self.barrier[sender] = CONNECTED
                if self.nb(CONNECTED) == len(self.barrier):
                    self.send_model()
                continue

            if recv == b'DONE':
                self.barrier[sender] = DONE
                if self.nb(DONE) == len(self.barrier):
                    self.disconnect()
                continue
            
            if recv == b'OVER':
                self.barrier[sender] = DISCONNECTED
                self.disconnect()
                if self.nb(DISCONNECTED) == len(self.barrier):
                    return # The only exit point
                continue

            j = json.loads(self.NT.decrypt(recv))
            meta, data = json.loads(j['meta']), json.loads(j['data'])
            data = util.deserialize_model(data)

            state_dict = self.ml.get_net().state_dict()
            for key, value in state_dict.items():
                state_dict[key] = (value + data[key]) / 2
            self.ml.get_net().load_state_dict(state_dict)

            for _ in range(self.local_updates):
                self.ml.step()
                if self.ml.get_epoch() == self.epochs:
                    self.notify_completion()
                    if self.nb(DONE) == len(self.barrier):
                        self.disconnect()
                    break

            #TODO verify that sending a model after DONE or OVER is not a problem
            self.send_model()


def start(rank, machine_id, procs_per_machine, machines, local_updates, ML, NT, epochs, *args):
    GossipNode(rank, machine_id, procs_per_machine, machines, local_updates, ML, NT, epochs, list(args)[0]).loop()

def run(machine_id, procs_per_machine, machines, local_updates, ML, NT, epochs, *args):
    spawn(fn=start, nprocs=procs_per_machine, 
        args=[machine_id, procs_per_machine, machines, local_updates, ML, NT, epochs] + list(args))
