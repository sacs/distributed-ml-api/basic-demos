FROM python:3.8

WORKDIR /user/demo

# Install dependencies
COPY requirements.txt .
RUN pip3 install -r requirements.txt
RUN pip3 install torch torchvision

# Copy source code
COPY data data
COPY algo_demos algo_demos

# Run application
CMD ["python3", "algo_demos/dpsgd.py"]
